# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval
from trytond.pool import Pool


_STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft')),
}
_DEPENDS = ['state']


class Sale(ModelSQL, ModelView):
    _name = 'sale.sale'

    text_modules_top = fields.One2Many('sale.sale.text_module_line',
            'sale_top', 'Text Modules Top', states=_STATES,
            depends=_DEPENDS)
    text_modules_bottom = fields.One2Many(
           'sale.sale.text_module_line', 'sale_bottom',
           'Text Modules Bottom', states=_STATES, depends=_DEPENDS)

    def __init__(self):
        super(Sale, self).__init__()
        self.party = copy.copy(self.party)
        if 'text_modules_bottom' not in self.party.on_change:
            self.party.on_change += ['text_modules_bottom']
        self.shipment_address = copy.copy(self.shipment_address)
        if not self.shipment_address.on_change:
            self.shipment_address.on_change = []
        if 'shipment_address' not in self.shipment_address.on_change:
            self.shipment_address.on_change += [
                'shipment_address', 'text_modules_bottom']
        self._rpc.update({'on_change_shipment_address': False})
        self._reset_columns()

    def on_change_party(self, vals):
        text_module_obj = Pool().get('text_module.text_module')

        res =  super(Sale, self).on_change_party(vals)
        add = []
        remove = []

        if vals.get('text_modules_bottom'):
            remove = self._get_text_modules_remove_ids(
                vals['text_modules_bottom'], 'party')

        if vals.get('party'):
            text_module_ids = text_module_obj.search([
                ('type', '=', 'party'),
                ('parties', '=', vals['party']),
                ('reports.model', '=', 'sale.sale'),
                ])
            if text_module_ids:
                text_modules = text_module_obj.browse(text_module_ids)
                for tm in text_modules:
                    add.append({
                        'text_module': tm.id,
                        'description': tm.text,
                        })

        vals2 = copy.copy(vals)
        vals2['shipment_address'] = res.get('shipment_address', False)
        res2 = self.on_change_shipment_address(vals2)
        add += res2['text_modules_bottom']['add']
        remove += res2['text_modules_bottom']['remove']

        res['text_modules_bottom'] = {'add': add, 'remove': remove}
        return res

    def _get_text_modules_remove_ids(self, tmvals, type):
        text_module_obj = Pool().get('text_module.text_module')

        res = []
        tmid2tmlineid = {}
        for tmline in tmvals:
            tmid2tmlineid.setdefault(tmline['text_module'], [])
            tmid2tmlineid[tmline['text_module']].append(tmline['id'])
        text_modules = text_module_obj.browse(
            [tm['text_module'] for tm in tmvals])
        for tm in text_modules:
            if tm.type == type:
                res += tmid2tmlineid[tm.id]
        return [x for x in set(res)]

    def on_change_shipment_address(self, vals):
        pool = Pool()
        address_obj = pool.get('party.address')
        text_module_obj = pool.get('text_module.text_module')

        add = []
        remove = []

        if vals.get('text_modules_bottom'):
            remove = self._get_text_modules_remove_ids(
                vals['text_modules_bottom'], 'country')

        if vals.get('shipment_address'):
            address = address_obj.browse(vals['shipment_address'])
            text_module_ids = text_module_obj.search([
                ('type', '=', 'country'),
                ('countries', '=', address.country.id),
                ['OR',
                    ('parties', '=', False),
                    ('parties', '=', address.party.id),
                ],
                ('reports.model', '=', 'sale.sale'),
                ])
            if text_module_ids:
                text_modules = text_module_obj.browse(text_module_ids)
                for tm in text_modules:
                    add.append({
                        'text_module': tm.id,
                        'description': tm.text,
                        })
        return {'text_modules_bottom': {'add': add, 'remove': remove}}

Sale()


class SaleTextModuleLine(ModelSQL, ModelView):
    'Sale Text Module Line'
    _name = 'sale.sale.text_module_line'
    _description = __doc__
    _rec_name = 'description'

    def __init__(self):
        super(SaleTextModuleLine, self).__init__()
        self._order.insert(0, ('sequence', 'ASC'))

    description = fields.Text('Description', required=True)
    text_module = fields.Many2One('text_module.text_module', 'Text Module',
        on_change=['text_module'],
        domain=[
            ('reports.model', '=', 'sale.sale'),
        ])
    sale_top = fields.Many2One('sale.sale', 'Sale Top')
    sale_bottom = fields.Many2One('sale.sale', 'Sale Bottom')
    sequence = fields.Integer('Sequence')

    def default_sequence(self):
        return 10

    def on_change_text_module(self, vals):
        text_module_obj = Pool().get('text_module.text_module')
        res = {}
        if vals.get('text_module'):
            text_module = text_module_obj.browse(vals['text_module'])
            res['description'] = text_module.text
            res['sequence'] = text_module.sequence
        return res

SaleTextModuleLine()


class SaleLine(ModelSQL, ModelView):
    _name = 'sale.line'

    text_module = fields.Many2One('text_module.text_module', 'Text Module',
            domain=[
                ('reports.model', '=', 'sale.sale'),
            ],
            states={
                'invisible': Not(Equal(Eval('type'), 'comment')),
            }, on_change=['text_module'], depends=['type'])

    def on_change_text_module(self, vals):
        text_module_obj = Pool().get('text_module.text_module')
        res = {}
        if vals.get('text_module'):
            text_module = text_module_obj.browse(vals['text_module'])
            res['description'] = text_module.text
        return res

SaleLine()

